const imgHost = "https://static.kuaimayoupin.com/000000_kt_xcx/home/home/";

// 为你匹配相关
export const you_avatar_mask = imgHost + "you_avatar_mask.png"
export const you_new_icon = imgHost + "you_new_icon.png";
export const you_pipei_3s = imgHost + "you_pipei_3s.png";
export const you_pipeizhi_bg = imgHost + "you_pipeizhi_bg.png";
export const kefu_icon = imgHost + "kefu_icon.png";
export const you_card_bg2 = imgHost + "you_card_bg2.png";
// 他们都在参与相关
export const ta_sex_man = imgHost + "ta_sex_man.png";
export const ta_sex_woman =  imgHost + "ta_sex_woman.png";
export const home_auth_icon = imgHost + "home_auth_icon.png";

// 发对象相关
export const home_duixiang_bg =  imgHost + "new_home_duixiang_bg.png";
export const qianxianshi =  imgHost + "qianxianshi.png";
export const mianfeifa =  imgHost + "mianfeifa_20.png";
export const people_avatar_border = imgHost + "people_avatar_border.png";
export const people_avatar = imgHost + "people_avatar.png";
export const sex_man_icon = imgHost + "sex_man_icon.png";
export const sex_woman_icon = imgHost + "sex_woman_icon.png";
export const zhuanfa_bg = imgHost + "zhuanfa_bg.png";
export const default_avatar = imgHost + "default_avatar.png";

// 导航
export const home_bar_bg =  imgHost + "home_bar_bg.png";
export const paifa_icon = imgHost + "paifa_icon.png";
export const home_tab_title = imgHost + "home_tab_title.png";






