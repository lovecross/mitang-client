import Vue from 'vue'
import App from './App'
import store from './store'
import {
	request
} from '@/util/request';
import uView from "uview-ui";
import moment from '@/util/moment/index.js'
// import TencentIM from '@/util/TencentIM.js';
import "@/util/permission";
import rfParser from '@/components/rf-parser';
import loginDialog from "@/components/login/login-dialog.vue";
import TabBar from "@/components/tabbar/index.vue"
Vue.component('TabBar', TabBar);  

Vue.use(uView);
Vue.component('rfParser', rfParser);
Vue.component('loginDialog', loginDialog);

Vue.prototype.$request = request;
// Vue.prototype.$im = TencentIM;
Vue.prototype.$moment = moment;
Vue.config.productionTip = false;
Vue.prototype.$store = store;

App.mpType = 'app'

Vue.filter('thumbnail', function(imgUrl) {
	return `${imgUrl}?imageView2/1/w/400/h/400`;
})
Vue.filter('moneyFormat', function(price) {
	return parseFloat(price).toFixed((2));
})

// 0待付款 1待发货 2已发货 3已收货 4已完成 -1退货申请 -2退款中 -3退款完成 -4已驳回 -5, 超时关闭   注：1/2/3都是待使用 
Vue.filter('orderFormat', function(order_status) {
	const status = parseInt(order_status);
	if(status === 0) return '待付款';
	else if(status === 1) return '待发货';
	else if(status === 2) return '已发货';
	else if(status === 3) return '已收货';
	else if(status === 4) return '已完成';
	else if(status === -1) return '退货申请';
	else if(status === -2) return '退款中';
	else if(status === -3) return '退款完成';
	else if(status === -4) return '已驳回';
	else if(status === -5) return '超时关闭';
	return '';
})


const app = new Vue({
	...App
})
app.$mount()


