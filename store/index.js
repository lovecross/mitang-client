import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate";
import userStore from './modules/user.js';
import tabbarStore from './modules/tabbar.js'
import chatStore from './modules/chat.js'
import printStore from './modules/print.js'
import homeStore from './modules/home.js';

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
	plugins: [
		createPersistedState({
			storage: {
				getItem: key => uni.getStorageSync(key),
				setItem: (key, value) => uni.setStorageSync(key, value),
				removeItem: key => uni.removeStorageSync(key)
			}
		})
	],
  modules: {userStore,tabbarStore,chatStore,printStore, homeStore}
})