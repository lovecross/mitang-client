
/**
 * 恋人订制 数据字典
 */

//  婚姻状况
const marital_status = [
    {
        label: '不限',
        value: -1,
        extra: "marital_status"
    },
    {
        label: '未婚',
        value: 1,
        extra: "marital_status"
    },
    {
        label: '离异',
        value: 2,
        extra: "marital_status"
    },
    {
        label: '丧偶',
        value: 3,
        extra: "marital_status"
    },
    {
        label: '已婚',
        value: 4,
        extra: "marital_status"
    },
]
// 星座
const constellation = [
    {
        label: '不限',
        rangeDate: '1.21-2.19',
        value: '-1'
    },
    {
        label: '水瓶座',
        rangeDate: '1.21-2.19',
        value: 'Aquarius'
    }, {
        label: '双鱼座',
        rangeDate: '2.20-3.20',
        value: 'Pisces'
    }, {
        label: '白羊座',
        rangeDate: '3.21-4.19',
        value: 'Aries'
    }, {
        label: '金牛座',
        rangeDate: '4.20-5.20',
        value: 'Taurus'
    }, {
        label: '双子座',
        rangeDate: '5.21-6.21',
        value: 'Gemini'
    }, {
        label: '巨蟹座',
        rangeDate: '6.22-7.22',
        value: 'Cancer'
    }, {
        label: '狮子座',
        rangeDate: '7.23-8.22',
        value: 'Leo'
    }, {
        label: '处女座',
        rangeDate: '8.23-9.22',
        value: 'Virgo'
    }, {
        label: '天秤座',
        rangeDate: '9.23-10.23',
        value: 'Libra'
    }, {
        label: '天蝎座',
        rangeDate: '10.24-11.21',
        value: 'Scorpio'
    }, {
        label: '人马座',
        rangeDate: '11.22-12.20',
        value: 'Sagittarius'
    }, {
        label: '山羊座',
        rangeDate: '12.21-1.20',
        value: 'Capricorn'
    }
]

// 教育水平
const education = [
    {
        label: '不限',
        value: -1,
        extra: "education"
    },
    {
        label: '高中及以下',
        value: 1,
        extra: "education"

    },
    {
        label: '专科',
        value: 2,
        extra: "education"
    },
    {
        label: '本科',
        value: 3,
        extra: "education"
    },
    {
        label: '硕士',
        value: 4,
        extra: "education"
    },
    {
        label: '博士及以上',
        value: 5,
        extra: "education"
    }
]
// 薪水
const salary = [
    {
      label: '不限',
      value: -1,
      extra: 'salary',
    },
    {
      label: '4千以下',
      value: 1,
      extra: 'salary',
    },
    {
      label: '4千－6千',
      value: 2,
      extra: 'salary',
    },
    {
      label: '6千－1万',
      value: 3,
      extra: 'salary',
    },
    {
      label: '1万－2万',
      value: 4,
      extra: 'salary',
    },
    {
      label: '2万－5万',
      value: 5,
      extra: 'salary',
    },
    {
      label: '5万以上',
      value: 6,
      extra: 'salary',
    },
  ];
  
// 工作类型
const work_type = [
    {
        label: '不限',
        value: -1
    },
    {
        label: '国企',
        value: 1
    },
    {
        label: '政府',
        value: 2
    },
    {
        label: '事业',
        value: 3
    },
    {
        label: '私企',
        value: 4
    },
    {
        label: '自主创业',
        value: 4
    },
]
// 住房状况
const housing = [
    {
        label: '不限',
        value: -1
    },
    {
        label: '已购房',
        value: 1
    },
    {
        label: '与父母同住',
        value: 2
    },
    {
        label: '租房',
        value: 3
    }
]
const ages = function () {
	var list = [];
	for( var index = 18; index <= 75; index++){
      list.push({value:index,label:index.toString(),extra:'age'});
    }
	return [list,list];
};
const heights = function(){
	var list = [];
	for( var index = 140; index <= 200; index++){
	  list.push({value:index,label:index.toString(),extra:'height'});
	}
	return [list,list];
};
const height = function(){
	var list = [];
	for( var index = 140; index <= 200; index++){
	  list.push({value:index,label:index.toString(),extra:'height'});
	}
	return list;
};
export default {
	marital_status,constellation,education,salary,work_type,housing,ages,heights,height
}