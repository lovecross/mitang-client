import {
    homeConnentRecommend,
} from "@/api/home/home.js";

// initial state
const state = {
    recommendList: [], // 首页推荐列表
    offset: 0,
    limit: 15,
    triggered: false,
    freshing: false,
}

// getters
const getters = {
    recommendList(state, getters) {
        return state.recommendList
    },
    freshing() {
        return state.freshing
    },
    triggered() {
        return state.triggered
    }
}

// actions
const actions = {
    getList({ state }, payload) {
        homeConnentRecommend({ limit: state.limit, offset: state.offset }).then((res) => {
            if (res.status == 1) {
                state.recommendList = res.data;
                state.triggered = false;
                state.freshing = false;
            }
        })
    },
    loadReset({ state }, payload) {
        homeConnentRecommend({ limit: state.limit, offset: 0 }).then((res) => {
            if (res.status == 1) {
                state.recommendList = res.data;
                state.offset = 0;
                state.triggered = false;
                state.freshing = false;
            }
        })
    },
    loadMore({ state }, payload) {
        let offset = parseInt(state.offset, 10) + 1;
        let oldList = state.recommendList
        homeConnentRecommend({ limit: state.limit, offset: offset }).then((res) => {
            if (res.status == 1) {
                state.recommendList = [...oldList, ...res.data];
                state.offset = offset;

                state.triggered = false;
                state.freshing = false;
            }
        })
    }
}

// mutations triggered
const mutations = {
    setRefresh(state, loading) { //设置参数
        state.freshing = loading;
    },
    setTrigger(state, triggered) { //设置参数
        state.triggered = triggered;
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}