const state = {
	
	vuex_tabbar: [{
			iconPath: "/static/home.png",
			selectedIconPath: "/static/home_active.png",
			text: '首页',
			isDot: false,
			customIcon: false,
			pagePath: "/pages/home2/home"
		},
		{
			
			iconPath: "/static/message.png",
			selectedIconPath: "/static/message_active.png",
			text: '消息',
			customIcon: false,
			pagePath: "/pages/message/message"
		},
		
		{
			iconPath: "/static/fabu.png",
			selectedIconPath: "/static/fabu.png",
			text: '',
			midButton: true,
			customIcon: false,
		},
		{
			iconPath: "/static/shop.png",
			selectedIconPath: "/static/shop_active.png",
			text: '趣玩',
			// count: 23,
			customIcon: false,
			pagePath: "/pages/mall/mall"

		},
		{
			iconPath: "/static/mine.png",
			selectedIconPath: "/static/mine_active.png",
			text: '我的',
			customIcon: false,
			pagePath: "/pages/mine/mine"
		}
	]
}

const actions = {};


const mutations = {
	setUnReadCount: (state,count)=>{
		state.vuex_tabbar[1].count = count
	}
}

export default {
	namespaced: true,
	state,
	actions,
	mutations
}
