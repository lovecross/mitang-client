const state = {
	hasLogin: false,
	userInfo: null,
	hasBindPhone: false,
	historySearch:null,
	searchData:[] //研究方向
}

const actions = {};


const mutations = {
	saveSearchData(state,searchData){
		state.searchData = searchData;
	},
	clearUserInfo(state, data){
		try{
			uni.removeStorageSync('userInfo');
			uni.removeStorageSync('openid');
			state.userInfo = null;
			state.hasLogin = false;
		}catch(e){
			//TODO handle the exception
		}
	},
	saveUserInfo(state, info){
		if(JSON.stringify(info) === '{}'){
			state.userInfo = {}
		}
		else{
			state.userInfo = Object.assign({} , state.userInfo , info);
		}
		// 本地存储
		try{			
			uni.setStorageSync('userInfo',state.userInfo);
		}catch(e){
			//TODO handle the exception
		}
		state.hasLogin = true;
		//判断 是否手机号 是否存在
		if(info.phone === null){
			uni.navigateTo({
				url:'/pages/registe-info/registe-info.vue'
			})
		}
		else{
			state.hasBindPhone = true;
		}
	},
	saveHistorySearch(state,data){
		state.historySearch = data
	},
	setLogin(state,loginState){
		state.hasLogin = loginState;
	}
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
} 