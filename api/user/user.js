import service from '@/util/request/request.js'
// 获取个人动态
export function talkingTipList(data){
	return service.request({
		method: "GET",
		url:"talkingTip/list",
		data
	})
}
// 完善个人信息
export function completeInformation(data){
	return service.request({
		method: "POST",
		url:"user/completeInformation",
		data
	})
}


// 实名认证
export function authUser(data){
	return service.request({
		method: "POST",
		url:"user/authUser",
		data
	})
}
// 获取兴趣标签
export function loadInterestLabels(data){
	return service.request({
		method: "GET",
		url:"user/loadInterestLabels",
		data
	})
}

export function customizeLoverInfo(data){
	return service.request({
		method: "GET",
		url:"customize/customizeLoverInfo",
		data
	})
}


// 获取性格标签
export function loadPersonalityLabels(data){
	return service.request({
		method: "GET",
		url:"user/loadPersonalityLabels",
		data
	})
}
// 获取个人信息
export function getInfo(data){
	return service.request({
		method: "GET",
		url:"user/getInfo",
		data
	})
}
//首页关注动态 列表
export function followed(data){
	return service.request({
		method: "GET",
		url:"talkingTip/followed",
		data
	})
}
// 首页推荐动态
export function recommend(data){
	return service.request({
		method: "GET",
		url:"talkingTip/recommend",
		data
	})
}
// 用户 动态、关注、粉丝、最近访客 统计
export function statistics(data){
	return service.request({
		method: "GET",
		url:"user/statistics",
		data
	})
}
// 身份证解析
export function idcard(data){
	return service.request({
		method: "POST",
		url:"ocr/idcard",
		data
	})
}
// 身份证解析
export function deleteDynamic(id){
	return service.request({
		method: "POST",
		url:"talkingTip/delete/"+id,
	})
}

// 获取vip
export function getVipList(data){
	return service.request({
		method: "GET",
		url:"member/vips",
		data
	})
}
// 最近认证用户列表
export function recentAuthUsers(){
	return service.request({
		method: "GET",
		url:"user/recentAuthUsers",
	})
}
// 购买会员
export function genOrder(data){
	return service.request({
		method: "POST",
		url:"member/genOrder",
		data
	})
}

// 获取会员信息
export function loadVipinfo(data){
	return service.request({
		method: "GET",
		url:"member/loadVipInfo",
		data
	})
}

// 经纬度解析
export function geocode(data){
	return service.request({
		method: "get",
		url:"geocode/regeo",
		data
	})
}


// 粉丝列表
export function getFansList(data){
	return service.request({
		method: "get",
		url:"followfans/fans_list",
		data
	})
}

export function getFollowList(data){
	return service.request({
		method: "get",
		url:"followfans/follow_list",
		data
	})
}
// 访客记录
export function record(data){
	return service.request({
		method: "post",
		url:"visitor/record",
		data
	})
}
// 访客列表
export function recent(data){
	return service.request({
		method: "get",
		url:"visitor/recent",
		data
	})
}
// 关注
export function follow(data){
	return service.request({
		method: "post",
		url:"followfans/follow",
		data
	})
}

// 关注
export function getCareerList(data){
	return service.request({
		method: "get",
		url:"career/list",
		data
	})
}
// 邀请记录
export function inviteRecord(data){
	return service.request({
		method: "get",
		url:"invite/record",
		data
	})
}
export function loadAuthInfo(data){
	return service.request({
		method: "get",
		url:"user/loadAuthInfo",
		data
	})
}
export function genCode(data){
	return service.request({
		method: "post",
		url:"invite/genCode",
		data
	})
}
export function getInviteInfo(data){
	return service.request({
		method: "get",
		url:"user/getInviteInfo?invite_no="+data,
		data
	})
}
export function updateLocationInfo(data){
	return service.request({
		method: "post",
		url:"user/position",
		data
	})
}