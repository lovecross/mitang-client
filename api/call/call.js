import service from '@/util/request/request.js'
// 打招呼列表
export function applyList(){
	return service.request({
		method: "GET",
		url:"chat/friend/applyList"
	})
}
// 处理打招呼
export function processApply(data){
	return service.request({
		method: "POST",
		url:"chat/friend/processApply",
		data
	})
}
// 搜索
export function search(data){
	return service.request({
		method: "POST",
		url:"chat/friend/search",
		data
	})
}

// 发出的招呼列表
export function apply(data){
	return service.request({
		method: "POST",
		url:"chat/friend/apply",
		data
	})
}