import service from '@/util/request/request.js'
// 获取匹配信息
export function getActivityParticipants(data){
	return service.request({
		method: "GET",
		url:"activity/participants",
		data
	})
}

// 获取匹配信息
export function getActivityInfo(){
	return service.request({
		method: "GET",
		url:"/activity/info",
	})
}

// 获取加微信剩余册数
export function getWechatCount(){
	return service.request({
		method: "GET",
		url:"/user/wechatCount",
	})
}

// 获取加微信剩余册数
export function getWechatSend(data){
	return service.request({
		method: "POST",
		url:"/user/sendWechat",
		data
	})
}