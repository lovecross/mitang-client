import service from '@/util/request/request.js'

// 创建59元牵线订单
export function makeMatchOrder(data){
	return service.request({
		method: "POST",
		url:"/order/matchOrder",
		data
	})
}

// 主动转发
export function activityShare(data){
	return service.request({
		method: "POST",
		url:"/activity/share",
		data
	})
}

// 主动转发
export function getWxPay(data){
	return service.request({
		method: "POST",
		url:"/order/wxpay",
		data
	})
}