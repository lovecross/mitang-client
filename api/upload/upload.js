import service from '@/util/request/request.js'

export function updateAuthInfo(data){
	return service.request({
		url:'user/updateAuthInfo',
		method:'POST',
		data
	})
} 