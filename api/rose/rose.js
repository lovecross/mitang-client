import service from '@/util/request/request.js'

// 获取玫瑰列表
export function roseList(data){
	return service.request({
		method: "GET",
		url:"rose/list",
		data
	})
}
// 获取玫瑰明细
export function transactions(data){
	return service.request({
		method: "GET",
		url:"rose/transactions",
		data
	})
}

// 获取玫瑰订单
export function genRoseOrder(data){
	return service.request({
		method: "POST",
		url:"rose/genOrder",
		data
	})
}

