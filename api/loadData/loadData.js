import service from '@/util/request/request.js'

export function loadData(data){
	return service.request({
		url:"address/loadData",
		method:"GET",
		data
	})
}