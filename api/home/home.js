import service from '@/util/request/request.js'
// 登录
export function publish(data){
	return service.request({
		method: "POST",
		url:"/talkingTip/publish/5f0865ef5911b0cb95e006c0",
		data
	})
}

// 发表
export function createTip(data){
	return service.request({
		method: "POST",
		url:"talkingTip/create",
		data
	})
}

export function apply(data){
	return service.request({
		method: "POST",
		url:"chat/friend/apply",
		data
	})
}



// 首页   动态 talking_count、关注 like_count、粉丝fans_count、最近访客 recent_visitors_count  个数 
export function statistics(){
	return service.request({
		method: "GET",
		url:"user/statistics"
	})
}
// 首页内容推荐
export function homeConnentRecommend(params){
	return service.request({
		method: "GET",
		url:"talkingTip/home",
		data: params
	})
}
// 获取 用户推荐列表
export function recommendUsers(){
	return service.request({
		method: "GET",
		url:"user/recommend"
	})
}
// 获取 首页关注 列表
export function followed(){
	return service.request({
		method: "GET",
		url:"talkingTip/followed"
	})
}
/// 给动态点赞
export function giveLike(data){
	return service.request({
		method: "POST",
		url:"talkingTip/giveLike",
		data
	})
}

// 获取定制恋人
export function getDing(){
	return service.request({
		method: "GET",
		url:"customize/hasCustomizeLover",
	})
}

/// 定制恋爱
export function customizeLover(data){
	return service.request({
		method: "POST",
		url:"customize/customizeLover",
		data
	})
}
/// 获取关注状态
export function getFollowStatus(data){
	return service.request({
		method: "GET",
		url:"followfans/isFollow",
		data
	})
}
///  关注
export function followRequest(data){
	return service.request({
		method: "POST",
		url:"followfans/follow",
		data
	})
}
/// 取消关注
export function followCancleRequest(data){
	return service.request({
		method: "POST",
		url:"followfans/cancel",
		data
	})
}

/// 获取个人信息 /user/getInfoById?user_id=816bb0d0-df8e-11ea-b8c0-33a85579023b
export function getInfoById(data){
	return service.request({
		method: "GET",
		url:"user/getInfoById",
		data
	})
}
///获取关注列表 talkingTip/listByUserId?user_id=816bb0d0-df8e-11ea-b8c0-33a85579023b
export function getTalkingTipListById(data){
	return service.request({
		method: "GET",
		url:"talkingTip/getTalkingTipsById",
		data
	})
}
/// 首页 获取最新的打赏消息 
export function getGiftsRecentRequest(data){
	return service.request({
		method: "GET",
		url:"gifts/recent",
		data
	})
}

/// 打赏礼物列表
export function getGiftsList() {
	return service.request({
		method: "GET",
		url:"gifts/list"
	})
}
/// 赠送礼物
export function sendGifts(data) {
	return service.request({
		method: "POST",
		url:"gifts/sendGifts",
		data
	})
}
/// 打招呼
/// user_id gift_id note
export function greetSend(data) {
	return service.request({
		method: "POST",
		url:"greet/send",
		data
	})
}
/// 举报 user/report
/// user_id tag pic_path note
export function userReport(data) {
	return service.request({
		method: "POST",
		url:"user/report",
		data
	})
}
/// 举报 user/addBlack
/// user_id
export function userAddBlack(data) {
	return service.request({
		method: "POST",
		url:"user/addBlack",
		data
	})
}
/// talkingTip/getByTalkingTipsId?id=

export function getTrendsDetail(data) {
	return service.request({
		method: "Get",
		url:"talkingTip/getByTalkingTipsId",
		data
	})
}


// 多少人参与 男女比例
export function activityInfo(data){
	return service.request({
		method: "GET",
		url:"activity/info",
		data
	})
}
// 参与列表
export function activityList(data){
	return service.request({
		method: "GET",
		url:"activity/list",
		data
	})
}
// 匹配列表
export function matchList(data){
	return service.request({
		method: "GET",
		url:"activity/matchList",
		data
	})
}
// 获取转发数量
export function activityShareInfo(data){
	return service.request({
		method: "GET",
		url:"activity/shareInfo",
		data
	})
}
// 转发
export function activityShare(data){
	return service.request({
		method: "POST",
		url:"activity/share",
		data
	})
}

// 报名
export function activityApply(data){
	return service.request({
		method: "POST",
		url:"activity/apply",
		data
	})
}