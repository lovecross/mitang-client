import TIM from 'tim-wx-sdk'; // 微信小程序环境请取消本行注释，并注释掉 import TIM from 'tim-js-sdk';
import COS from 'cos-wx-sdk-v5'; // 微信小程序环境请取消本行注释，并注释掉 import COS from 'cos-js-sdk-v5';
import { REFRESH_CONVERSATIONS } from "@/util";

// 创建 SDK 实例，TIM.create() 方法对于同一个 SDKAppID 只会返回同一份实例
var arr = [];
let options = {
	SDKAppID: 1400446038  //  1400452864 接入时需要将0替换为您的即时通信应用的 SDKAppID
};
let tim = TIM.create(options); // SDK 实例通常用 tim 表示
// 设置 SDK 日志输出级别，详细分级请参见 setLogLevel 接口的说明
// tim.setLogLevel(0); // 普通级别，日志量较多，接入时建议使用
tim.setLogLevel(1); // release级别，SDK 输出关键信息，生产环境时建议使用

// 将腾讯云对象存储服务 SDK （以下简称 COS SDK）注册为插件，IM SDK 发送文件、图片等消息时，需要用到腾讯云的 COS 服务
// HTML5 环境，注册 COS SDK

// 微信小程序环境，注册 COS SDK
tim.registerPlugin({
	'cos-wx-sdk': COS
}); // 微信小程序环境请取消本行注释，并注释掉 tim.registerPlugin({'cos-js-sdk': COS});

// 监听事件，如：
tim.on(TIM.EVENT.SDK_READY, function(event) {
	// 收到离线消息和会话列表同步完毕通知，接入侧可以调用 sendMessage 等需要鉴权的接口
	// console.log('收到离线消息');
	// console.log(event)
	uni.$emit(REFRESH_CONVERSATIONS)
});

tim.on(TIM.EVENT.MESSAGE_RECEIVED, function(event) {
	// console.log('新消息');
	arr.push(event);
	// 收到推送的单聊、群聊、群提示、群系统通知的新消息，可通过遍历 event.data 获取消息列表数据并渲染到页面
	// event.name - TIM.EVENT.MESSAGE_RECEIVED
	// event.data - 存储 Message 对象的数组 - [Message]
	uni.$emit(REFRESH_CONVERSATIONS)
});

tim.on(TIM.EVENT.MESSAGE_REVOKED, function(event) {
	// 收到消息被撤回的通知。使用前需要将SDK版本升级至v2.4.0或更高版本。
	// event.name - TIM.EVENT.MESSAGE_REVOKED
	// event.data - 存储 Message 对象的数组 - [Message] - 每个 Message 对象的 isRevoked 属性值为 true
});

tim.on(TIM.EVENT.MESSAGE_READ_BY_PEER, function(event) {
	// SDK 收到对端已读消息的通知，即已读回执。使用前需要将SDK版本升级至v2.7.0或更高版本。仅支持单聊会话。
	// event.name - TIM.EVENT.MESSAGE_READ_BY_PEER
	// event.data - event.data - 存储 Message 对象的数组 - [Message] - 每个 Message 对象的 isPeerRead 属性值为 true
});

tim.on(TIM.EVENT.CONVERSATION_LIST_UPDATED, function(event) {
	// 收到会话列表更新通知，可通过遍历 event.data 获取会话列表数据并渲染到页面
	// event.name - TIM.EVENT.CONVERSATION_LIST_UPDATED
	// event.data - 存储 Conversation 对象的数组 - [Conversation]
});

tim.on(TIM.EVENT.GROUP_LIST_UPDATED, function(event) {
	// 收到群组列表更新通知，可通过遍历 event.data 获取群组列表数据并渲染到页面
	// event.name - TIM.EVENT.GROUP_LIST_UPDATED
	// event.data - 存储 Group 对象的数组 - [Group]
});

tim.on(TIM.EVENT.PROFILE_UPDATED, function(event) {
	// 收到自己或好友的资料变更通知
	// event.name - TIM.EVENT.PROFILE_UPDATED
	// event.data - 存储 Profile 对象的数组 - [Profile]
});

tim.on(TIM.EVENT.BLACKLIST_UPDATED, function(event) {
	// 收到黑名单列表更新通知
	// event.name - TIM.EVENT.BLACKLIST_UPDATED
	// event.data - 存储 userID 的数组 - [userID]
});

tim.on(TIM.EVENT.ERROR, function(event) {
	// 收到 SDK 发生错误通知，可以获取错误码和错误信息
	// event.name - TIM.EVENT.ERROR
	// event.data.code - 错误码
	// event.data.message - 错误信息
	console.log('收到 SDK 发生错误通知', event.name);
});

tim.on(TIM.EVENT.SDK_NOT_READY, function(event) {
	// 收到 SDK 进入 not ready 状态通知，此时 SDK 无法正常工作
	// event.name - TIM.EVENT.SDK_NOT_READY
});

tim.on(TIM.EVENT.KICKED_OUT, function(event) {
	// 收到被踢下线通知
	// event.name - TIM.EVENT.KICKED_OUT
	// event.data.type - 被踢下线的原因，例如 :
	//   - TIM.TYPES.KICKED_OUT_MULT_ACCOUNT 多实例登录被踢
	//   - TIM.TYPES.KICKED_OUT_MULT_DEVICE 多终端登录被踢
	//   - TIM.TYPES.KICKED_OUT_USERSIG_EXPIRED 签名过期被踢（v2.4.0起支持）。
});

tim.on(TIM.EVENT.NET_STATE_CHANGE, function(event) {
	// 网络状态发生改变（v2.5.0 起支持）。
	// event.name - TIM.EVENT.NET_STATE_CHANGE
	// event.data.state 当前网络状态，枚举值及说明如下：
	//   - TIM.TYPES.NET_STATE_CONNECTED - 已接入网络
	//   - TIM.TYPES.NET_STATE_CONNECTING - 连接中。很可能遇到网络抖动，SDK 在重试。接入侧可根据此状态提示“当前网络不稳定”或“连接中”
	//   - TIM.TYPES.NET_STATE_DISCONNECTED - 未接入网络。接入侧可根据此状态提示“当前网络不可用”。SDK 仍会继续重试，若用户网络恢复，SDK 会自动同步消息
});

const trimUserId = userID => {
	return userID.replace(/-/g, "");
}
let timObj = {
	getMessage:() =>{
		return arr;
	},
	login: () => {
		const userSig = uni.getStorageSync('tencent_im_sign');
		const userInfo = uni.getStorageSync('userInfo');
		// console.log('TencentIM.login userInfo' + JSON.stringify(userInfo));
		if (userSig && userInfo) {
			return tim.login({
				userID: trimUserId(userInfo.user_id),
				userSig: userSig
			});
		}
		return Promise.reject();
	},
	// 发送纯文本消息
	sendMessage:(hellow,data)=>{
		console.log(data.user_id);
		console.log(data.user_id.replace(/-/g,''))
		let message = tim.createTextMessage({
		  to: data.user_id.replace(/-/g,''),
		  conversationType: TIM.TYPES.CONV_C2C,
		  payload: {
		    text: hellow
		  }
		});
		let promise = tim.sendMessage(message);
		promise.then(function(imResponse) {
			console.log('发送成功')
		  // 发送成功
		  console.log(imResponse);
		}).catch(function(imError) {
		  // 发送失败
		  console.warn('sendMessage error:', imError);
		});
	},
	// 发送图片
	sendImgMessage:(imgData,data)=>{
		console.log(data.user_id);
		console.log(data.user_id.replace(/-/g,''))
		let message = tim.createImageMessage({
		  to: data.user_id.replace(/-/g,''),
		  conversationType: TIM.TYPES.CONV_C2C,
		  payload: { file: imgData },
		});
		let promise = tim.sendMessage(message);
		promise.then(function(imResponse) {
		  // 发送成功
		  console.log(imResponse);
		}).catch(function(imError) {
		  // 发送失败
		  console.warn('sendMessage error:', imError);
		});
	},
	// 发送录音
	sendAudioMessage:(audioData,data)=>{
		console.log(data.user_id);
		console.log(data.user_id.replace(/-/g,''))
		let message = tim.createAudioMessage({
		  to: data.user_id.replace(/-/g,''),
		  conversationType: TIM.TYPES.CONV_C2C,
		  payload: { file: audioData },
		});
		let promise = tim.sendMessage(message);
		promise.then(function(imResponse) {
		  // 发送成功
		  console.log(imResponse);
		}).catch(function(imError) {
		  // 发送失败
		  console.warn('sendMessage error:', imError);
		});
	},
	logout: () => {
		return tim.logout();
	},
	getMessageList: (userid,nextReqMessageID=1) => {
		console.log('nextReqMessageID',nextReqMessageID)
		if(nextReqMessageID==1){
			return tim.getMessageList({conversationID: `C2C${userid}`, count: 15});
		}else{
			return tim.getMessageList({conversationID: `C2C${userid}`,nextReqMessageID, count: 15});
		}	
	},
	getConversationList: () => {
		// 拉取会话列表
		return tim.getConversationList();
		// promise.then(function(imResponse) {
		//   const conversationList = imResponse.data.conversationList; // 会话列表，用该列表覆盖原有的会话列表
		//   return conversationList
		// }).catch(function(imError) {
		//   console.warn('getConversationList error:', imError); // 获取会话列表失败的相关信息
		// });
	},
	setMessageRead: (userid) => {
		return tim.setMessageRead({conversationID:`C2C${userid}`});
	},
}

module.exports = timObj