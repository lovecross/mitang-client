import moment from 'dayjs';
export const chat_time = time => {
	return moment(time).format('MM-DD HH:mm');
}

export const transfromtTime = time => {
	var d = new Date(moment(time).format());
	var times = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate() + ' ' + (d.getHours()<10?'0'+d.getHours():d.getHours()) + ':' + (d.getMinutes()<10?'0'+d.getMinutes():d.getMinutes()) +
		':' + (d.getSeconds()<10?'0'+d.getSeconds():d.getSeconds());
	return times
}
