import {
	getInfo
} from '@/api/user/user.js'

export function getUserInfo() {

	let promise = new Promise((resolve, reject) => {
		getInfo().then(res => {
			if (res.status == 1) {
				getApp().globalData.userInfo = res.data
				uni.setStorageSync('userInfo', res.data);
				resolve(res.data)
			} else {
				reject({
					"msg": res.message
				})
			}
			
		})
	})
	return promise
}
