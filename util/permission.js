//拦截器 拦截只要是未登录状态，想要跳转到名单内的路径时，直接跳到登录页。

// uni.addInterceptor('switchTab', {
// 	// tabbar页面跳转前进行拦截
// 	invoke (e) {
// 		console.log(e)
// 		if (uni.getStorageSync('token')) {
// 			const userInfo = uni.getStorageSync("userInfo") || {};
// 			if (userInfo.nick_name.length===0) {
// 				uni.navigateTo({url: '/pageA/my-profile/my-profile-once'})
// 				return false;
// 			}
// 			return true
// 		}
// 		else{
// 			uni.$emit("showLogin");
// 			return false;
// 		}
// 	},
// 	success (e) {
// 		// console.log(e)
// 	}
// })
