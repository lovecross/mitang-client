// 正式线 https://casita.net.cn/wxs/
// export const baseUrl = 'https://www.casita.net.cn/wxs/';
// export const loginUrl = 'https://www.casita.net.cn/cotrunWx/wxApi/wxLogin';
// export const socketUrl ='wss://casita.net.cn/wss/'
// export const fullpath = src => {
// 	return baseUrl  + src;
// }

//测试线
export const baseUrl = 'https://www.casita.net.cn/wx/';
export const loginUrl = 'https://www.casita.net.cn/cotrunWx/wxApi/wxLogin';
export const socketUrl = 'wss://casita.net.cn/ws/'
export const fullpath = src => {
	return baseUrl + src;
}

// 腾讯云im 相关事件
export const REFRESH_CONVERSATIONS = 'REFRESH_CONVERSATIONS';