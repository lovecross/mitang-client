import moment from '@/util/moment/moment.js'
export function getAges(str) {
	var r = str.match(/^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2})$/);
	if (r == null) return '';
	var d = new Date(r[1], r[3] - 1, r[4]);
	if (d.getFullYear() == r[1] && (d.getMonth() + 1) == r[3] && d.getDate() == r[4]) {
		var Y = new Date().getFullYear();
		return ((Y - r[1]) + "岁");
	}
	return ("0岁");
}

export function getAgesYear(str) {
	if(str.length < 4){
		return '';
	}
	let year = str.substring(2,4);
	return year + "年";
}

export function isEmpty(obj) {
	if (typeof obj == "undefined" || obj == null || obj == "") {
		return true;
	} else {
		return false;
	}
}
export function jsDateDiff(publishTime) {
	
	var d_minutes, d_hours, d_days;
	var timeNow = parseInt(new Date().getTime() / 1000);
	var d;
	d = timeNow - moment(publishTime).unix();
	d_days = parseInt(d / 86400);
	d_hours = parseInt(d / 3600);
	d_minutes = parseInt(d / 60);
	if (d_days > 0 && d_days <= 1) {
		return d_days + "天前";
	} else if (d_days <= 0 && d_hours > 0) {
		return d_hours + "小时前";
	} else if (d_hours <= 0 && d_minutes > 0) {
		return d_minutes + "分钟前";
	} else if (d_minutes <= 0 && d > 5 ) {
		return d + "秒前";
	} else if (d <= 5 ) {
		return "刚刚";
	} else {
		return moment(publishTime).format('MM/DD');
	}
}
/// 计算生肖
export function calculateSX(birthday){
	//计算出生肖
	var year = moment(birthday).format('YYYY');
	var arr1 = ['猴', '鸡', '狗', '猪', '鼠', '牛', '虎', '兔', '龙', '蛇', '马', '羊'];
	var result = /^\d{4}$/.test(year) ? arr1[year % 12] : false;
	return  "属" + result;
} 
export function calculateXZ(birthday){

	var year = moment(birthday).format('YYYY');
	//计算出星座
	var month = moment(birthday).format('MM');
	var day = moment(birthday).format('DD');
	var s = "魔羯水瓶双鱼牡羊金牛双子巨蟹狮子处女天秤天蝎射手魔羯";
	var arr = [20,19,21,21,21,22,23,23,23,23,22,22];
	var result = s.substr(month*2-(day<arr[month-1]?2:0),2);
	return result
} 